//console.log("JS DOM - Manipulation")

//[Section] Documents Object Model(DOM)
	//allows us to access or modify the properties of an html element in a webpage
	//it is a standard on how to get, change, add or delete HTML elements
	//we will be focusing only with DOM in terms of managing forms.

	//For selecting HTML elements we will be using document.querySelectorAll / getElementById.
		//Syntax: document.querySelector("html element") 
		
		//CSS selectors
			//class selector (.)
			//id selector (#)
			//tag selector(html tags)
			//universal selector(*)
			//attribute selector([attribute])
	//querySelectorAll
	let universalSelector = document.querySelectorAll("*")
	console.log(universalSelector)

	//querySelector
	let singleUniversal = document.querySelector("*")
	console.log(singleUniversal)

	let classSelector = document.querySelectorAll(".full-name")
	console.log(classSelector)

	let singleClassSelector = document.querySelector(".full-name")
	console.log(singleClassSelector)

	let tagSelector = document.querySelectorAll("input")
	console.log(tagSelector)

	let spanSelector = document.querySelector("span[id]")
	console.log(spanSelector)


	//getElement
		let element = document.getElementById("fullName")
		console.log(element)

//[Section] Event Listeners
	//whenever a user interacts with a webpage, this action is considered as an event
	//working with events is large part of creating interactivity in a web page
	//specific funtion that will be triggered if the event happen.

	//The function used is "addEventListener", it takes two arguments
		//first argument a string identifying the event
		//second argument, function that the listener will trigger once the "specified event" occur.

	let txtFirstName = document.querySelector("#txt-first-name");
	

	//add eventlistener
	txtFirstName.addEventListener("keyup", () => {
		console.log(txtFirstName.value);
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})

	
	let txtLastName = document.querySelector("#txt-last-name");
	txtLastName.addEventListener("keyup", () => {
		spanSelector.innerHTML = ` ${txtFirstName.value} ${txtLastName.value}`
	})


const changeColor = document.querySelector('#text-color');

changeColor.addEventListener('change', (event) => {

	console.log(event.target.value)
 	if(event.target.value === ""){
 		spanSelector.style.backgroundColor = "black"
 	}else{
 		spanSelector.style.backgroundColor = `${event.target.value}`;
 	}
  
});


